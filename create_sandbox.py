import sys
import os
import argparse
import subprocess
import hashlib
import yaml
import requests

from generator.var_generator import generate
from generator.var_parser import parser_var_file


class SandboxCreator:
    """
        A class to represent a sandbox creator.
        ...

        Attributes
        ----------
        var_file_name : String
            file containing variables data required for generating

        Methods
        -------
        get_cwd(relative_path):
            Static function to get correct absolut path to given relative path.

        get_variables(variables_file_path):
             Static function to load variables data from file.

        is_file_openable(path):
            Static function to check if path navigates to openable file.

        post_req(url, data, num=10):
            Static function to post data to CTFd server using request.

        run_generator(self, seed):
            Function to generated variables using seed to setup random generator.

        create_temp_file(self, val_dic):
            Function to create temporary file containing generated variables data.

        get_randomized_vagrant_command(self):
            Function returns command to correctly set up generated variables into Sandbox.

        upload_flags(self, url):
            Function to upload flags from .generated.yml file to CTFd server.

        create_dic(self):
            Function to create dictionary of variable names and their challenge IDs.

        create(self):
            Function to build sandbox.

        """
    var_file_name = "variables.yml"

    def __init__(self, input_arg, generated_file_name=".generated", win_tmp="tmp.ps1"):
        """
               Initialize all the necessary attributes and build sandbox.

               Parameters
               ----------
               input_arg : Namespace
                    input arguments
               generated_file_name : str
                    name of temporary generated file
               win_tmp : str
                    name of temporary script for windows
               """
        if "@" in input_arg.mail:
            self.generated_file_name = generated_file_name + "_" \
                                       + input_arg.mail.split("@")[0] + ".yml"
        else:
            self.generated_file_name = generated_file_name + ".yml"
        self.input_arg = input_arg
        self.win_script_name = win_tmp
        self.create()

    @staticmethod
    def get_cwd(relative_path):
        """
            Static function to get correct absolut path to given relative path.

            Parameters
                ----------
                relative_path : String
                    String relative path

            Returns
                -------
                String
                    String absolut path

            """
        return os.path.join(os.path.abspath(os.path.dirname(__file__)), relative_path)

    @staticmethod
    def get_variables(variables_file_path):
        """
            Static function to load variables data from file.

            Parameters
                ----------
                variables_file_path : String
                    relative path to variables file

            Returns
                -------
                List
                    Variables instances

        """
        with open(SandboxCreator.get_cwd(variables_file_path)) as file:
            variables_list = parser_var_file(file)
        if variables_list is None:
            raise Exception("Sandbox is corrupted or misses some files.")
        return variables_list

    @staticmethod
    def is_file_openable(path):
        """
            Static function to check if path navigates to openable file.

            Parameters
                ----------
                path : String
                    relative/absolut path to file

            Returns
                -------
                Bool
                    is openable

        """
        try:
            with open(os.path.join(SandboxCreator.get_cwd(path), SandboxCreator.var_file_name))\
                    as _:
                pass
        except:
            return False
        return True

    @staticmethod
    def post_req(url, data, num=10):
        """
            Static function to post data to CTFd server using request.

            Parameters
                ----------
                url : String
                    url address of CTFd server

                data : dict
                    data to post

                num : int
                    number of tries to upload data

            Returns
                -------
                Bool
                    was it successful
        """

        errors = []
        for i in range(num):
            try:
                result = requests.post(url + "/store", data=data).json()
                if result["success"] or result["uploaded"]:
                    break
                errors.append(result["message"])
                if i == num - 1:
                    raise Exception("upload error")
            except:
                print("An error occurred, check your internet connection.",
                      file=sys.stderr)
                print("Errors : ", file=sys.stderr)
                if not errors:
                    # CTFd is not responding
                    print("CTFd error.",
                          file=sys.stderr)
                for current_err in errors:
                    if "FOREIGN KEY (`challenge_id`)" in current_err:
                        print("Challenge " + current_err.split("'")[5] + " is missing in CTFd!",
                              file=sys.stderr)
                        return False
                    if "FOREIGN KEY (`user_id`)" in current_err or \
                            "User doesn't exist." in current_err:
                        print("User " + data["user_email"] + " is missing in CTFd!",
                              file=sys.stderr)
                        return False
                    print("Unknown CTFd error", file=sys.stderr)
                return False
        return True

    def run_generator(self, seed):
        """
                    Function to generated variables using seed to setup random generator.

                    Parameters
                        ----------
                        seed : int
                            int relative path to variables file

                    Returns
                        -------
                        dictionary
                            name of the variable as key and generate value as value

        """
        variable_list = self.get_variables(os.path.join(self.input_arg.path,
                                                        SandboxCreator.var_file_name))
        return generate(variable_list, seed)

    def create_temp_file(self, val_dic):
        """
            Function to create temporary file containing generated variables data.

            Parameters
                ----------
                val_dic : dict
                    generated variable names and values

            Returns
                -------
                None
        """
        generated_tuples_as_text = " "
        for key, value in val_dic.items():
            generated_tuples_as_text += key + ' : ' + '"' + value + '"' + '\n '
        try:
            with open(os.path.join(self.input_arg.path, self.generated_file_name), "w") \
                    as generated_file:
                generated_file.write(generated_tuples_as_text)
        except:
            if os.path.isfile(os.path.join(self.input_arg.path, self.generated_file_name)):
                # file has already existed and couldn't be overwritten, therefore continue
                return
            raise Exception("Fatal error")

        if os.name == 'nt':
            subprocess.check_call(["attrib", "+H",
                                   os.path.join(self.input_arg.path, self.generated_file_name)])

    def get_randomized_vagrant_command(self):
        """
                    Function returns command to correctly set up generated variables into Sandbox.

                    Parameters
                        ----------

                    Returns
                        -------
                        String
                            linux shell command if it runs on linux based OS otherwise None

        """
        player_seed = int(hashlib.sha1(self.input_arg.mail.encode("utf-8")).hexdigest(), 16) \
                      % (10 ** 8)
        val_dic = self.run_generator(player_seed)
        self.create_temp_file(val_dic)

        if os.name == 'nt':
            vagrant_cwd = f'$Env:VAGRANT_CWD="{self.input_arg.path}"'
            ansible_args = '$Env:ANSIBLE_ARGS=\'--extra-vars "@' + self.generated_file_name + '"\''

            with open('tmp.ps1', 'w') as out_file:
                out_file.write(vagrant_cwd)
                out_file.write('\n')
                out_file.write(ansible_args)
                out_file.write('\n')
                out_file.write('vagrant up')

        else:
            result_command = 'VAGRANT_CWD="' + self.input_arg.path + '"'
            result_command += ' ANSIBLE_ARGS=\'--extra-vars "@' + self.generated_file_name + \
                              '"\' vagrant up  '
            return result_command
        return None

    def upload_flags(self, url):
        """
            Function to upload flags from .generated.yml file to CTFd server.

            Parameters
                ----------
                url : String
                    url address of CTFd server

            Returns
                -------
                int
                    number of flags that were not uploaded
        """
        failed_flags = 0
        challenge_dic = self.create_dic()
        with open(os.path.join(self.get_cwd(self.input_arg.path), self.generated_file_name)) \
                as file:
            data = dict()
            flags_data = yaml.full_load(file)
            data["user_email"] = self.input_arg.mail
            for flag in flags_data.keys():
                if not challenge_dic.get(flag):
                    continue
                data["flag"] = flags_data[flag]
                data["challenge_id"] = challenge_dic[flag]
                if not self.post_req(url, data):
                    failed_flags += 1
        return failed_flags

    def create_dic(self):
        """
                    Function to create dictionary of variable names and their challenge IDs.

                    Parameters
                        ----------

                    Returns
                        -------
                        dict
                            variable names as keys and challenge IDs as values
        """
        with open(self.get_cwd(os.path.join(self.input_arg.path, SandboxCreator.var_file_name)))\
                as file:
            var_list = yaml.load(file, Loader=yaml.FullLoader)
            result = {}
            for var in var_list.keys():
                try:
                    for _ in range(len(var_list[var])):
                        challenge_id = var_list[var].get("challenge_id")
                        if challenge_id is not None:
                            result[var] = challenge_id
                            break
                except:
                    raise Exception("An error occurred during opening file with the sandbox.")
        return result

    def create(self):
        """
                    Function to build sandbox.

                    Parameters
                        ----------

                    Returns
                        -------
                        None
        """
        if not os.path.isdir(self.input_arg.path):
            raise Exception(
                "Sandbox does not exist or you have entered wrong path: " + self.input_arg.path +
                "\n Check the path provided by '-p path'.")

        command = self.get_randomized_vagrant_command()

        if not self.input_arg.dry_run:
            print("Starting a sandbox... ")
            if os.path.isfile(self.win_script_name):
                subprocess.call(["powershell.exe", "./" + self.win_script_name], stdout=sys.stdout)
            else:
                os.system(command)

        self.upload_flags(self.input_arg.url)
        if os.path.isfile(self.win_script_name):
            os.remove(self.win_script_name)
        if os.path.isfile(os.path.join(self.input_arg.path, self.generated_file_name)):
            os.remove(os.path.join(self.input_arg.path, self.generated_file_name))


def parse_input():
    """
                Function to parse input arguments.

                Parameters
                    ----------

                Returns
                    -------
    """
    my_parser = argparse.ArgumentParser()
    my_parser.version = '1.0'
    my_parser.add_argument('-p', '--path', action='store', default="sandbox",
                           help='Path to the sandbox')
    my_parser.add_argument('-u', '--url', action='store', help='URL of the CTFd server',
                           default="https://game.kypo.muni.cz")
    my_parser.add_argument('-d', '--dry-run', action='store_true', help=argparse.SUPPRESS)
    required_arg = my_parser.add_argument_group('required arguments')
    required_arg.add_argument('-m', '--mail', action='store', required=True,
                              help='Player\'s login email')
    return my_parser.parse_args()


if __name__ == "__main__":
    print("Loading ...")
    SandboxCreator(parse_input())
