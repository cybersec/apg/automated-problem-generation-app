# Generation app

This application uses the [generator library](https://gitlab.fi.muni.cz/cybersec/apg/automated-problem-generation-lib) for creating a personalized sandbox for a hands-on lab. The personalized sandbox is created based on a template, which is filled with pseudorandomly generated values using the generator library and a definition of variable values in the `variables.yml` file.

This application and the generator library implements steps 2 and 3 in the following figure:
![toolset scheme](./toolset.png)

## Prerequisites

1. [Git](https://git-scm.com).
2. [Pipenv](https://github.com/pypa/pipenv).
3. Installed [CTFd with the Personal Challenge plugin](https://gitlab.fi.muni.cz/cybersec/apg/ctfd-personal_challenge-plugin).
4. Initialized CTFd with the `admin` account and accounts of students who will be provided with personalized sandboxes:
   1. Go to CTFd [Setup](http://localhost:8000/setup) and Administration tab. 
   2. Set up `admin` account. 
   3. Leave other fields blank and finish the setup. 
   4. Create student accounts in the Admin panel.
5. Created personal challenges with IDs corresponding to the variable definiton file `variables.yml` in a sandbox definition.

If you want to test this app with [an example sandbox](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/secret-laboratory), import [the saved export of CTFd database](ctfd-db.zip) at [CTFd Config -> Backup -> Import](http://localhost:8000/admin/config). The database contains five personal challenges, one account `student` with mail `student@example.org` and password `student`, and the `admin` account with `admin` password.

## Installation

### Linux (Ubuntu/Debian)

1. Clone this project: `git clone https://gitlab.fi.muni.cz/cybersec/apg/automated-problem-generation-app.git`
2. Navigate to the project directory: `$ cd automated-problem-generation-app`
3. Install [generator](https://gitlab.fi.muni.cz/cybersec/apg/automated-problem-generation-lib) and other required libraries with `$ pipenv install`

## Usage

After the installation, run the command `$ pipenv run python create_sandbox.py [-h] [-d] [-p <sandbox_path>] [-u <CTFd Server URL>] -m <User mail>` to generate values and store them to your CTFd server.

### Required arguments
```
 -m  --mail    student's mail registered in CTFd
```

### Optional arguments
```
-p --path      path to sandbox
-u --url       CTFd URL address
-d --dry-run   do not build game environment
-h --help      show help message and exit
```

#### Default values of optional arguments:
```
-p --path = sandbox
-u --url = https://game.kypo.muni.cz
-d --dry-run = False
```

### Example

1. To test this app with [an example sandbox](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/secret-laboratory), download it first:
`git clone https://gitlab.ics.muni.cz/muni-kypo-trainings/games/secret-laboratory.git`

2. Before you run the command below, make sure that:
- path in `-p` argument is the corrrect path to the sandbox 
- the CTFd server is up and running at URL provided in `-u` the argument
- the user with the mail in `-m` exists at the CTFd server
- challenges with IDs specified in the [variables definition](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/secret-laboratory/-/blob/master/variables.yml) exist at the CTFd server
`$ pipenv run python3 create_sandbox.py --dry-run -p secret-laboratory -u http://localhost:8000 -m student@example.org`

3. If everything works, you should see `Loading ...` at the output and individual flags of personal challenges 2, 3, and 5 for `student@example.org` stored at CTFd server (see e.g. http://localhost:8000/admin/challenges/2 -> Flags).

4. You can then run the command **without** `--dry-run` and build the sandbox with the individual flags:
`$ pipenv run python3 create_sandbox.py -p secret-laboratory -u http://localhost:8000 -m student@example.org`
You should see `Starting a sandbox...` and the ouput of Vagrant at the standard output.

5. When Vagrant finishes (it takes a while), connect to the `server` machine via SSH (`vagrant ssh server`) and check the individual flag with ID 2 for `student` has been inserted into the first line of a text file: `$ sudo cat /home/chong/READ-ME.txt`

## How does it work?

First, `create_sandbox.py` parses a definition of variables in `variables.yml` in the sandbox directory specified by `-p`. Then it uses the [generator](https://gitlab.fi.muni.cz/cybersec/apg/automated-problem-generation-lib) library for generating pseudorandom values of variables based on a seed (`-m`) and start the sandbox using [Vagrant](https://www.vagrantup.com). Finally, the script stores the generated values to a CTFd instance running at URL specified by `-u`.
